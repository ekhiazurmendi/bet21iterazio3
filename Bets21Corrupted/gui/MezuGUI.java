package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import businessLogic.BLFacade;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ResourceBundle;
import java.awt.event.ActionEvent;

import domain.*;

public class MezuGUI extends JFrame {

	private JPanel contentPane;
	public static MezuGUI Megui;
	private BLFacade BLogic= MainGUI.getBusinessLogic(); 
	private Mezua selectedMezu;
	private DefaultComboBoxModel<String> MezuModel = new DefaultComboBoxModel<String>();
	private List<Mezua> MezuList = BLogic.getMezuak((Bezeroa)LoginGUI.logeatua);
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MezuGUI frame = new MezuGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MezuGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton OnartuButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Onartu"));
		
		JButton UkatuButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Ukatu"));
		
		OnartuButton.setEnabled(false);
		UkatuButton.setEnabled(false);
		
		JButton CloseButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Itzuli"));
		CloseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserGUI.usergui.setVisible(true);
				dispose();
			}
		});
		CloseButton.setBounds(166, 227, 89, 23);
		contentPane.add(CloseButton);
		
		JComboBox MezuBox = new JComboBox();
		MezuBox.setMaximumRowCount(10); 
		
		for (Mezua m : MezuList) {
			MezuModel.addElement(m.getTestua());
		}
		
		
		MezuBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int i= MezuBox.getSelectedIndex();
				selectedMezu = MezuList.get(i);
				if(selectedMezu!=null) {
				OnartuButton.setEnabled(true);
				UkatuButton.setEnabled(true);
				}
			}
		});
		MezuBox.setBounds(78, 11, 263, 20);
		contentPane.add(MezuBox);
		
		
		OnartuButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {		
				LoginGUI.logeatua = BLogic.mezuaErantzun(selectedMezu, true, (Bezeroa)LoginGUI.logeatua);
				OnartuButton.setEnabled(false);
				UkatuButton.setEnabled(false);

			}
		});
		OnartuButton.setBounds(78, 193, 89, 23);
		contentPane.add(OnartuButton);
		

		UkatuButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginGUI.logeatua = BLogic.mezuaErantzun(selectedMezu, false, (Bezeroa)LoginGUI.logeatua);
				OnartuButton.setEnabled(false);
				UkatuButton.setEnabled(false);
	
			}
		});
		UkatuButton.setBounds(252, 193, 89, 23);
		contentPane.add(UkatuButton);
		
		JLabel MezuLabel = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Mezuak"));
		MezuLabel.setBounds(10, 14, 52, 14);
		contentPane.add(MezuLabel);
	}
}
