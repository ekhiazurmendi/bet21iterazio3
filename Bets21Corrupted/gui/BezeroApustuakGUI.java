package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import businessLogic.BLFacade;
import domain.Apustua;
import domain.Bezeroa;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BezeroApustuakGUI extends JFrame {
	
	public static BezeroApustuakGUI bezeroapustuakgui;
	
	private JTable tableApustuak = new JTable();

	private DefaultTableModel tableModelApustuak;
	
	private JPanel contentPane;
	
	private String[] columnNamesApustuak = new String[] { ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Apustua"),
			ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Data"), ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Galdera")
	};
	
	private Bezeroa bezero;
	
	private List<Apustua> apustuak;
	
	private Apustua selectedApustua;
	
	private JButton btnApustuaEzabatuButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ApustuaEzabatu"));
	
	private JLabel lblNireApustuakLabel = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("NireApustuak"));
	
	private JButton btnItzuliButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Itzuli"));
	
	private JScrollPane scrollPane = new JScrollPane();
	
	private JLabel lblErrorLabel = new JLabel("");
	private final JButton btnEmaitzarikGabeButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("EmaitzarikGabe")); //$NON-NLS-1$ //$NON-NLS-2$
/*
	/**
	 * Launch the application.
	 
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BezeroApustuakGUI frame = new BezeroApustuakGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
*/
	/**
	 * Create the frame.
	 */
	public BezeroApustuakGUI() {
		try {
			jbInit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void jbInit() throws Exception {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 528, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		bezero=(Bezeroa)LoginGUI.logeatua;
		BLFacade facade = MainGUI.getBusinessLogic();
		
		
		lblErrorLabel.setBounds(352, 205, 150, 14);
		contentPane.add(lblErrorLabel);
		
		btnItzuliButton.setBounds(352, 182, 150, 23);
		contentPane.add(btnItzuliButton);
		btnItzuliButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserGUI.usergui.setVisible(true);
				dispose();
			}
		});
		
		scrollPane.setBounds(26, 49, 316, 201);
		contentPane.add(scrollPane);
		
		
		btnApustuaEzabatuButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = tableApustuak.getSelectedRow();
				facade.deleteApustu(selectedApustua);
				selectedApustua.apustuaezabatu();
				tableModelApustuak.removeRow(i);
			}
		});
		btnApustuaEzabatuButton.setEnabled(false);
		btnApustuaEzabatuButton.setBounds(352, 80, 150, 23);
		contentPane.add(btnApustuaEzabatuButton);
		
		lblNireApustuakLabel.setBounds(26, 24, 238, 14);
		contentPane.add(lblNireApustuakLabel);
		
		scrollPane.setViewportView(tableApustuak);
		tableModelApustuak = new DefaultTableModel(null, columnNamesApustuak);
		tableApustuak.setModel(tableModelApustuak);
		
		JButton btnApustuGuztiakButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ApustuGuztiak")); //$NON-NLS-1$ //$NON-NLS-2$
		btnApustuGuztiakButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				apustuak =(List) bezero.getApustuL();
				if(!(apustuak.isEmpty()) || apustuak!=null) {
					tableModelApustuak.setColumnCount(3);
					tableModelApustuak.setDataVector(null, columnNamesApustuak);
			
					for (domain.Apustua k : apustuak) {
							Vector<Object> row = new Vector<Object>();
				
							//System.out.println("Kuotak " + k);
				
							row.add(k.getBet());
							row.add(k.getData());
							row.add(facade.getGaldera(k.getKuota()).getQuestion());
							//row.add(k); // ev object added in order to obtain it with tableModelEvents.getValueAt(i,2)
							tableModelApustuak.addRow(row);
					}
					System.out.println(tableApustuak.getColumnModel().getColumnCount());
					tableApustuak.getColumnModel().getColumn(0).setPreferredWidth(100);
					tableApustuak.getColumnModel().getColumn(1).setPreferredWidth(100);
					tableApustuak.getColumnModel().getColumn(2).setPreferredWidth(268);
				}else {
					lblErrorLabel.setText(ResourceBundle.getBundle("Etiquetas").getString("EzDuzuApusturik"));
				}
			}
		});
		btnApustuGuztiakButton.setBounds(352, 114, 150, 23);
		contentPane.add(btnApustuGuztiakButton);
		btnEmaitzarikGabeButton.setBounds(352, 148, 150, 23);
		
		btnEmaitzarikGabeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				apustuak =(List) bezero.getApustuL();
				if(!(apustuak.isEmpty()) || apustuak!=null) {
					tableModelApustuak.setColumnCount(3);
					tableModelApustuak.setDataVector(null, columnNamesApustuak);
			
					for (domain.Apustua k : apustuak) {
						if(k.getAmaituta()==false) {
							Vector<Object> row = new Vector<Object>();
				
							//System.out.println("Kuotak " + k);
				
							row.add(k.getBet());
							row.add(k.getData());
							row.add(facade.getGaldera(k.getKuota()).getQuestion());
							//row.add(k); // ev object added in order to obtain it with tableModelEvents.getValueAt(i,2)
							tableModelApustuak.addRow(row);
						}
					}
					System.out.println(tableApustuak.getColumnModel().getColumnCount());
					tableApustuak.getColumnModel().getColumn(0).setPreferredWidth(100);
					tableApustuak.getColumnModel().getColumn(1).setPreferredWidth(100);
					tableApustuak.getColumnModel().getColumn(2).setPreferredWidth(268);
				}else {
					lblErrorLabel.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("EzDuzuApusturik"));
				}
			}
		});
		
		contentPane.add(btnEmaitzarikGabeButton);
		apustuak =(List) bezero.getApustuL();
		//try {
			if(!(apustuak.isEmpty()) || apustuak!=null) {
				tableModelApustuak.setColumnCount(3);
				tableModelApustuak.setDataVector(null, columnNamesApustuak);
		
				for (domain.Apustua k : apustuak) {
					if(k.getAmaituta()==false) {
						Vector<Object> row = new Vector<Object>();
			
						//System.out.println("Kuotak " + k);
			
						row.add(k.getBet());
						row.add(k.getData());
						row.add(facade.getGaldera(k.getKuota()).getQuestion());
						//row.add(k); // ev object added in order to obtain it with tableModelEvents.getValueAt(i,2)
						tableModelApustuak.addRow(row);
					}
				}
				System.out.println(tableApustuak.getColumnModel().getColumnCount());
				tableApustuak.getColumnModel().getColumn(0).setPreferredWidth(100);
				tableApustuak.getColumnModel().getColumn(1).setPreferredWidth(100);
				tableApustuak.getColumnModel().getColumn(2).setPreferredWidth(268);
			}else {
				lblErrorLabel.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("EzDuzuApusturik"));
			}
		//}catch(NullPointerException z){
			//lblErrorLabel.setText("Ez duzu apusturik");
		//}
		
		
		
		tableApustuak.getColumnModel().getColumn(0).setPreferredWidth(100);
		tableApustuak.getColumnModel().getColumn(1).setPreferredWidth(100);
		tableApustuak.getColumnModel().getColumn(2).setPreferredWidth(268);
		
		tableApustuak.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int i = tableApustuak.getSelectedRow();
				selectedApustua = apustuak.get(i);
				btnApustuaEzabatuButton.setEnabled(true);
			}
		});
	}
}
