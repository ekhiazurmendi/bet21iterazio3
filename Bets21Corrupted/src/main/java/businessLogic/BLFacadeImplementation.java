package businessLogic;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.jws.WebMethod;
import javax.jws.WebService;

import configuration.ConfigXML;
import dataAccess.DataAccess;
import domain.*;
import exceptions.EventAlreadyExist;
import exceptions.EventFinished;
import exceptions.QuestionAlreadyExist;
import gui.LoginGUI;

/**
 * It implements the business logic as a web service.
 */
@WebService(endpointInterface = "businessLogic.BLFacade")
public class BLFacadeImplementation implements BLFacade {
	DataAccess dbManager;

	public BLFacadeImplementation() {
		System.out.println("Creating BLFacadeImplementation instance");
		ConfigXML c = ConfigXML.getInstance();

		if (c.getDataBaseOpenMode().equals("initialize")) {
			dbManager = new DataAccess(c.getDataBaseOpenMode().equals("initialize"));
			// dbManager.initializeDB();
			dbManager.close();
		}
		dbManager = new DataAccess(c.getDataBaseOpenMode().equals("initialize"));
	}

	public BLFacadeImplementation(DataAccess da) {

		System.out.println("Creating BLFacadeImplementation instance with DataAccess parameter");
		ConfigXML c = ConfigXML.getInstance();

		if (c.getDataBaseOpenMode().equals("initialize")) {
			da.open(true);
			da.initializeDB();
			da.close();

		}
		dbManager = da;
	}

	/**
	 * This method creates a question for an event, with a question text and the
	 * minimum bet
	 * 
	 * @param event      to which question is added
	 * @param question   text of the question
	 * @param betMinimum minimum quantity of the bet
	 * @return the created question, or null, or an exception
	 * @throws EventFinished        if current data is after data of the event
	 * @throws QuestionAlreadyExist if the same question already exists for the
	 *                              event
	 */
	@WebMethod
	public Question createQuestion(Event event, String question, float betMinimum)
			throws EventFinished, QuestionAlreadyExist {

		// The minimum bed must be greater than 0
		dbManager.open(false);
		Question qry = null;

		if (new Date().compareTo(event.getEventDate()) > 0)
			throw new EventFinished(ResourceBundle.getBundle("Etiquetas").getString("ErrorEventHasFinished"));

		qry = dbManager.createQuestion(event, question, betMinimum);

		dbManager.close();

		return qry;
	};

	public Pertsona isLogin(String erabiltzailea, String pasahitza) {
		dbManager.open(false);
		Pertsona p = dbManager.isLogin(erabiltzailea, pasahitza);
		dbManager.close();
		return p;
	}

	public boolean erregistratu(String dni, String herrialdea, String izena, String abizena, Date jaiotzeData,
			String korreoa, String erabiltzailea, String pasahitza, String kontuKorrontea, boolean kopiatuBai) {
		boolean ongi = false;
		dbManager.open(false);
		if (dbManager.libreDago(erabiltzailea) == null) {
			ongi = dbManager.erregistratu(dni, herrialdea, izena, abizena, jaiotzeData, korreoa, erabiltzailea,
					pasahitza, kontuKorrontea, kopiatuBai);
		} else {
			
		}
		dbManager.close();
		return ongi;
	}

	public boolean createEvent(Integer eventNumber, String description, Date eventDate) throws EventAlreadyExist {
		dbManager.open(false);
		if (dbManager.eventExist(description, eventDate) == null) {
			dbManager.createEvent(eventNumber, description, eventDate);
			dbManager.close();
			return true;
		}
		dbManager.close();
		return false;
	}

	public boolean updateDirua(Bezeroa bezero, float diruKop, Date date, String desk) {
		dbManager.open(false);
		boolean a = this.dbManager.updateDirua(bezero, diruKop, date, desk);
		dbManager.close();
		return a;

	}

	public void deleteEvent(Event event) {
		dbManager.open(false);
		this.ezabatuEvent(event);
		this.dbManager.deleteEvent(event);
		dbManager.close();
	}

	private void ezabatuEvent(Event event) {
		Apustua apustu=null;
		Event eventa = dbManager.getEvent(event);
		for (Question a : eventa.getQuestions()) {
				for (Kuota b : a.getKuotak()) {
					Iterator<Apustua> it= b.getApustuL().iterator();
					try {
					while(it.hasNext()) {
						apustu=it.next();
							dbManager.deleteApustua(apustu);
					}
					}catch(Exception e) {
						
					}
				}
		}
	}

	/**
	 * This method invokes the data access to retrieve the events of a given date
	 * 
	 * @param date in which events are retrieved
	 * @return collection of events
	 */
	@WebMethod
	public Vector<Event> getEvents(Date date) {
		dbManager.open(false);
		Vector<Event> events = dbManager.getEvents(date);
		dbManager.close();
		return events;
	}

	/**
	 * This method invokes the data access to retrieve the dates a month for which
	 * there are events
	 * 
	 * @param date of the month for which days with events want to be retrieved
	 * @return collection of dates
	 */
	@WebMethod
	public Vector<Date> getEventsMonth(Date date) {
		dbManager.open(false);
		Vector<Date> dates = dbManager.getEventsMonth(date);
		dbManager.close();
		return dates;
	}

	public void close() {
		DataAccess dB4oManager = new DataAccess(false);

		dB4oManager.close();

	}

	/**
	 * This method invokes the data access to initialize the database with some
	 * events and questions. It is invoked only when the option "initialize" is
	 * declared in the tag dataBaseOpenMode of resources/config.xml file
	 */
	@WebMethod
	public void initializeBD() {
		dbManager.open(false);
		dbManager.initializeDB();
		dbManager.close();
	}

	public void deleteApustu(Apustua apustu) {
		dbManager.open(false);
		this.dbManager.deleteApustua(apustu);
		dbManager.close();
	}

	@WebMethod
	public int createKuota(Question q, float k, String em) {
		dbManager.open(false);
		boolean libre = dbManager.kuotaLibre(q, k, em);
		int ongi = 1; // 0=dena ongi 1=ez dago libre 2=erroreak
		if (libre)
			if (dbManager.createKuota(q, k, em))
				ongi = 0;
			else
				ongi = 2;
		dbManager.close();
		return ongi;
	}

	@WebMethod
	public boolean emaitzaIpini(Question q, String em) {
		dbManager.open(false);
		boolean b = dbManager.emaitzaIpini(q, em);
		dbManager.close();
		return b;
	}

	@WebMethod
	public Bezeroa createApustua(Float bet, Date data, Bezeroa b, List<Kuota> k, boolean grupala) {
		dbManager.open(false);
		Bezeroa be = dbManager.createApustua(bet,data, b, k);
		if(grupala) {
			Apustua azkena = be.getLastApustu();
			for(Bezeroa l:be.getLagunak()) {
				dbManager.mezuaBidali(l,new MezuGrupala(be,l,new Date()),azkena);
			}
		}
		dbManager.close();
		return be;
	}

	@WebMethod
	public Question getGaldera(Kuota kuota) {
		dbManager.open(false);
		Question q = dbManager.getGaldera(kuota);
		dbManager.close();
		return q;
	}

	@WebMethod
	public Bezeroa mezuaErantzun(int mz, boolean o, Bezeroa u) {
		Bezeroa b=null;
		this.dbManager.open(false);
		Mezua m= this.dbManager.getMezu(mz);
		if(!o) {
				b=this.dbManager.ezabatuMezua(m, u);
				this.dbManager.close();
				return b;
		}else {
			if(m instanceof MezuLaguna) {
				b=this.dbManager.onartuMezua((MezuLaguna)m, u);
				this.dbManager.close();
				return b;
			}
			if(m instanceof MezuGrupala) {
				b=this.dbManager.onartuMezua((MezuGrupala)m, u);
				this.dbManager.close();
				return b;
			}if(m==null) {

			}
		}
		return b;
	}

	@WebMethod
	public boolean lagunaEgin(Bezeroa u, String erab) {
		this.dbManager.open(false);
		Mezua m= this.dbManager.lagunEskaeraEgin(u, erab);
		if(m==null) {
			this.dbManager.close();
			return false;
		}else {
			this.dbManager.close();
			return true;
		}
	}

	@WebMethod
	public boolean kopiatuEgin(Bezeroa u, String erab, float z) {
		this.dbManager.open(false);
		Bezeroa e= (Bezeroa)this.dbManager.bilatuErabiltzailea(erab);
		Bezeroa u1= (Bezeroa)this.dbManager.bilatuErabiltzailea(u.getErabiltzailea());
		for(Kopiatu a:u1.getaaab()) {
			if(a.getKopiatzailea().equals(e)) {
				this.dbManager.close();
				return false;
			}
		}
		if(e==null) {
			this.dbManager.close();
			return false;
		}else {
			Kopiatu k=this.dbManager.kopiatuEgin(u1, e, z);
			this.dbManager.close();
			if(k==null) {
				return false;
			}else {
				return true;
			}
		}
	}
	
	@WebMethod
	public Bezeroa updateBezero(Bezeroa u) {
		this.dbManager.open(false);
		Bezeroa a=(Bezeroa)this.dbManager.bilatuErabiltzailea(u.getErabiltzailea());
		this.dbManager.close();
		return a;
	}
	@WebMethod
	public List<Mezua> getMezuak(Bezeroa u){
		this.dbManager.open(false);
		Bezeroa a=(Bezeroa)this.dbManager.bilatuErabiltzailea(u.getErabiltzailea());
		this.dbManager.close();
		return a.getMezuak();
	}
	@WebMethod
	public List<Bezeroa> getBezeroak(String erab){
		this.dbManager.open(false);
		List<Bezeroa> bezeroak=this.dbManager.bilatuErabiltzaileak(erab);
		this.dbManager.close();
		return bezeroak;
	}

	@Override
	public Pertsona updateUser(String string) {
		this.dbManager.open(false);
		Pertsona a=this.dbManager.bilatuErabiltzailea(string);
		this.dbManager.close();
		return a;
	}

	@Override
	public List<Bezeroa> getLagunak(String erab) {
		this.dbManager.open(false);
		Bezeroa a=(Bezeroa) this.dbManager.bilatuErabiltzailea(erab);
		this.dbManager.close();
		return a.getLagunak();
	}

	@Override
	public boolean kopiaDaiteke(String erabiltzailea) {
		this.dbManager.open(false);
		Bezeroa a=(Bezeroa) this.dbManager.bilatuErabiltzailea(erabiltzailea);
		boolean a1= a.isKopiatzerikBai();
		this.dbManager.close();
		return a1;
	}

}
