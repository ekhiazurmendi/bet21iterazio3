package domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
public class Kuota implements Serializable{
	@XmlID
	@XmlJavaTypeAdapter(IntegerAdapter.class)
	@Id @GeneratedValue
	private Integer kuotaZenbakia;
	
	private float kuota;
	private String erantzunP;
	@XmlIDREF
	private Question galdera;
	@XmlIDREF
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private List<Apustua> apustuL;
	
	public Kuota() {
		
	}
	
	public Kuota(float kuota, String erantzunP, Question galdera) {
		this.galdera=galdera;
		this.kuota = kuota;
		this.erantzunP = erantzunP;
		this.apustuL=new Vector<Apustua>();
	}
	public Question getGaldera() {
		return galdera;
	}
	public void setGaldera(Question galdera) {
		this.galdera = galdera;
	}
	public float getKuota() {
		return kuota;
	}
	public void setKuota(float kuota) {
		this.kuota = kuota;
	}
	public String getErantzunP() {
		return erantzunP;
	}
	public void setErantzunP(String erantzunP) {
		this.erantzunP = erantzunP;
	}
	public void addapustu(Apustua apustu) {
		this.apustuL.add(apustu);
	}
	public List<Apustua> getApustuL() {
		return apustuL;
	}
	public Apustua addNewApustua(Apustua a) {
		apustuL.add(a);
		return a;
	}
	public Integer getKuotaZenbakia() {
		return kuotaZenbakia;
	}
	public void setKuotaZenbakia(Integer kuotaZenbakia) {
		this.kuotaZenbakia = kuotaZenbakia;
	}
}
