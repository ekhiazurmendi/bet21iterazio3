package domain;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@Entity
public class Kopiatu implements Serializable{
	@XmlID
	@XmlJavaTypeAdapter(IntegerAdapter.class)
	@Id @GeneratedValue
	private Integer id;
	private float zenbat;
	@OneToMany(fetch=FetchType.EAGER)
	private Bezeroa kopiatu;
	@OneToMany(fetch=FetchType.EAGER)
	private Bezeroa kopiatzailea;
	
	
	
	public Kopiatu(Bezeroa kopiatzailea, Bezeroa aaaa, float zenbat) {
		this.setZenbat(zenbat);
		this.setKopiatu(aaaa);
		this.setKopitzailea(kopiatzailea);
	}
	
	public Kopiatu() {
		super();
	}




	public Integer getId() {
		return id;
	}




	public void setId(Integer id) {
		this.id = id;
	}




	public void setKopiatzailea(Bezeroa kopiatzailea) {
		this.kopiatzailea = kopiatzailea;
	}




	public float getZenbat() {
		return zenbat;
	}

	public void setZenbat(float zenbat) {
		this.zenbat = zenbat;
	}

	public Bezeroa getKopiatu() {
		return kopiatu;
	}

	public void setKopiatu(Bezeroa kopiatu) {
		this.kopiatu = kopiatu;
	}

	public Bezeroa getKopiatzailea() {
		return kopiatzailea;
	}

	public void setKopitzailea(Bezeroa kopiatzailea) {
		this.kopiatzailea = kopiatzailea;
	}

}
