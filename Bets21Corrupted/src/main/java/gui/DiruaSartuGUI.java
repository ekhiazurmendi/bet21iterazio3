package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import businessLogic.BLFacade;
import domain.Bezeroa;
import domain.Langilea;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class DiruaSartuGUI extends JFrame {
	public static DiruaSartuGUI diruaSartuGUI;
	private BLFacade BLogic = MainGUI.getBusinessLogic();
	private JPanel contentPane;
	private JTextField DiruKopField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DiruaSartuGUI frame = new DiruaSartuGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DiruaSartuGUI() {
		diruaSartuGUI = this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 616, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		DiruKopField = new JTextField();
		DiruKopField.setBounds(27, 72, 207, 58);
		contentPane.add(DiruKopField);
		DiruKopField.setColumns(10);

		JLabel lblNewLabel = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("SartuNahi"));
		lblNewLabel.setBounds(252, 90, 151, 23);
		contentPane.add(lblNewLabel);

		JButton SartuButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("DiruaSartu"));
		SartuButton.setBounds(27, 151, 207, 46);
		contentPane.add(SartuButton);

		JButton CloseButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Itxi"));
		CloseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginGUI.logeatua =(Bezeroa)MainGUI.getBusinessLogic().updateUser(LoginGUI.logeatua.Erabiltzailea);
				UserGUI.usergui.setVisible(true);
				dispose();
			}
		});
		CloseButton.setBounds(145, 227, 99, 23);
		contentPane.add(CloseButton);
		
		JLabel lblErrorLabel = new JLabel();
		lblErrorLabel.setBounds(252, 167, 338, 14);
		contentPane.add(lblErrorLabel);
		
		JLabel lblDiruaLabel = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Dirua")+": "+((Bezeroa)(LoginGUI.logeatua)).getDirua());
		lblDiruaLabel.setBounds(27, 40, 207, 14);
		contentPane.add(lblDiruaLabel);
		SartuButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				float diruKop = Float.parseFloat(DiruKopField.getText());
				if (0 > diruKop) {
					if (((Bezeroa) LoginGUI.logeatua).getDirua() >= -diruKop) {
						((Bezeroa) LoginGUI.logeatua).updateDirua(diruKop, new Date(), "Dirua Atera");
						BLogic.updateDirua((Bezeroa) LoginGUI.logeatua, diruKop, new Date(), "Dirua Atera");
					}else {
						lblErrorLabel.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("DiruGutxi"));
					}
				}else if(diruKop > 0){
					((Bezeroa) LoginGUI.logeatua).updateDirua(diruKop, new Date(), "Dirua Sartu");
					BLogic.updateDirua((Bezeroa) LoginGUI.logeatua, diruKop, new Date(), "Dirua Sartu");
				}else {
					lblErrorLabel.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Sartu0H"));
				}
				lblDiruaLabel.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Dirua")+": "+((Bezeroa)(LoginGUI.logeatua)).getDirua());
			}
		});

	}
}
