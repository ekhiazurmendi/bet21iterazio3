package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import businessLogic.BLFacade;
import domain.Bezeroa;
import domain.Mezua;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;
import java.awt.event.ActionEvent;

public class LagunGUI extends JFrame {

	public static LagunGUI lagui;
	private BLFacade BLogic= MainGUI.getBusinessLogic(); 
	private List<Bezeroa> BezeroList= new Vector<Bezeroa>();
	private int UserIndex;
	private DefaultComboBoxModel<String> UserModel = new DefaultComboBoxModel<String>();
	private Bezeroa selectedUser;
	private boolean ondo;
	private boolean ondo2;
	private JPanel contentPane;
	private JTextField UserField;
	private JTextField KopiField;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LagunGUI frame = new LagunGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LagunGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton closeButon = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Itzuli"));
		closeButon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginGUI.logeatua =(Bezeroa)MainGUI.getBusinessLogic().updateUser(LoginGUI.logeatua.Erabiltzailea);
				UserGUI.usergui.setVisible(true);
				dispose();
			}
		});
		closeButon.setBounds(123, 227, 165, 23);
		contentPane.add(closeButon);
		
		JLabel ZenbatLabel = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ZenbatKopiatu"));
		ZenbatLabel.setBounds(56, 180, 279, 14);
		contentPane.add(ZenbatLabel);
		
		JLabel ErrorLabel = new JLabel("");
		ErrorLabel.setBounds(10, 125, 103, 14);
		contentPane.add(ErrorLabel);
		ErrorLabel.setForeground(Color.RED);
		
		JComboBox UserBox = new JComboBox();
		JButton LagunButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("LagunEgin"));
		JButton KopiatuButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Kopiatu"));
		
		LagunButton.setEnabled(false);
		KopiatuButton.setEnabled(false);
		
		UserField = new JTextField();
		UserField.setBounds(123, 45, 147, 20);
		contentPane.add(UserField);
		UserField.setColumns(10);
		
		JButton BilatuButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Bilatu"));
		BilatuButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				/*
				UserBox.removeAll();
				UserModel.removeAllElements();
				BezeroList.clear();
				*/
				BezeroList = BLogic.getBezeroak(UserField.getText());
				for (Bezeroa b : BezeroList) {
					if(!b.getErabiltzailea().equals(LoginGUI.logeatua.getErabiltzailea())) {
							UserModel.addElement(b.getErabiltzailea());
					}
				}
				if(BezeroList.contains(LoginGUI.logeatua)) {
					BezeroList.remove(LoginGUI.logeatua);
				}
				UserBox.setModel(UserModel);
				ErrorLabel.setText("");
			}
		});
		BilatuButton.setBounds(10, 44, 103, 23);
		contentPane.add(BilatuButton);
		
		
		
		
		UserBox.setMaximumRowCount(10); 
		
		UserBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserIndex = UserBox.getSelectedIndex();
				selectedUser = BezeroList.get(UserIndex);
				if(selectedUser!=null) {
					LagunButton.setEnabled(true);
					KopiatuButton.setEnabled(true);
				}
			}
		});
		UserBox.setBounds(290, 45, 134, 20);
		contentPane.add(UserBox);
		
		
		LagunButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Bezeroa> lagunak=BLogic.getLagunak(LoginGUI.logeatua.getErabiltzailea());
				if(!lagunak.contains(selectedUser)) {
					ondo = BLogic.lagunaEgin((Bezeroa)LoginGUI.logeatua, selectedUser.getErabiltzailea());
					if(!ondo) {
						ErrorLabel.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("LagunError"));
					}else {
						ErrorLabel.setText("Ongi");
					}
					LagunButton.setEnabled(false);
				}else {
					ErrorLabel.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("LagunError"));
				}
			}
		});
		LagunButton.setBounds(56, 97, 103, 23);
		contentPane.add(LagunButton);
		
		KopiField = new JTextField();
		KopiField.setBounds(345, 177, 67, 20);
		contentPane.add(KopiField);
		KopiField.setColumns(10);
		
		KopiatuButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserIndex = UserBox.getSelectedIndex();
				selectedUser = BezeroList.get(UserIndex);
				try {
					float zenbat = Float.parseFloat(KopiField.getText())/100;
					if(zenbat<=0) {
						ErrorLabel.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ZenbakiaSartu"));
						return;
					}
					if(BLogic.kopiaDaiteke(selectedUser.getErabiltzailea())) {
						ondo2 = BLogic.kopiatuEgin((Bezeroa)LoginGUI.logeatua, selectedUser.getErabiltzailea(), zenbat);
						if(!ondo2) {
							ErrorLabel.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("KopiatuError"));
						}else {
							ErrorLabel.setText("");
						}
					}else {
						ErrorLabel.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("EzinKopia"));
					}
				}catch(java.lang.NumberFormatException er) {
					ErrorLabel.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ZenbakiaSartu"));
				}
				
				
				
				KopiatuButton.setEnabled(false);
			}
		});
		KopiatuButton.setBounds(56, 146, 103, 23);
		contentPane.add(KopiatuButton);
		
	
		
		
		
		
	}
}
